import java.util.ArrayList;

public class ContoCorrente {
    private double saldo;
    private boolean contoBloccato;
    private ArrayList<Movimento> movimenti;

    public ContoCorrente(double saldoIniz) {
        this.saldo = saldoIniz;
        this.contoBloccato = false;
        this.movimenti = new ArrayList<>();
    }

public double getSaldo() {
    if (contoBloccato) {
        System.out.println("CONTO BLOCCATO!");
        return 0;
    } else {
        return saldo;
    }
}

public void prelievo(Movimento movimento) {
        double importoPrelievo = movimento.getImporto(0);
        if (!contoBloccato && importoPrelievo > 0 && importoPrelievo <= saldo) {
            saldo -= importoPrelievo;
            System.out.println("Prelievo avvenuto con successo. Saldo rimanente: " + saldo);

            movimento.setDataRichiesta(0);
            movimento.setValuta("Euro (€)", 0);
            movimento.setDescrizione("Prelievo", 0);
            movimento.setTipologia("Prelievo", 0);

            movimenti.add(movimento);
        } else {
            System.out.println("Importo non valido o saldo insufficiente.");
        }
    }

public void deposito(double importoDeposito) {
    if (!contoBloccato && importoDeposito > 0) {
        saldo += importoDeposito;
        System.out.println("Deposito avvenuto con successo. Nuovo saldo: " + saldo);

        Movimento movimento = new Movimento(1);
        movimento.setDataRichiesta(0);
        movimento.setValuta("Euro (€)", 0);
        movimento.setDescrizione("Deposito", 0);
        movimento.setTipologia("Deposito", 0);

        movimenti.add(movimento);
    } else {
        System.out.println("Importo non valido per il deposito.");
    }
}


public void bloccaConto() {
    contoBloccato = true;
    System.out.println("Conto bloccato.");
}

public void sbloccaConto() {
    contoBloccato = false;
    System.out.println("Conto sbloccato.");
}
public void stampaMovimenti() {
        System.out.println("Movimenti del conto:");
        for (Movimento movimento : movimenti) {
            System.out.println("Data: " + movimento.getDataRichiesta(0) +
                               ", Valuta: " + movimento.getValuta(0) +
                               ", Descrizione: " + movimento.getDescrizione(0) +
                               ", Tipologia: " + movimento.getTipologia(0) +
                               ", Importo: " + movimento.getImporto(0));
        }
    }
}
