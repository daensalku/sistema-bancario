import java.util.Scanner;
import java.time.LocalDate;

public class TerminaleBanca {
  public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);

        ContoCorrente cSiric = new ContoCorrente(2500);
        System.out.println("Saldo: " + cSiric.getSaldo());

        System.out.println("Inserisci \n 1. Per prelevare\n 2. Per depositare");
        int scelta = scanner.nextInt();

        switch(scelta){
            case 1:
                System.out.println("Inserisci l'importo da prelevare: ");
                int importoPrelievo = scanner.nextInt(); //double non funziona
                Movimento movimentoPrelievo = new Movimento(importoPrelievo);

                movimentoPrelievo.setDataRichiesta(0);
                movimentoPrelievo.setValuta("Valuta: ", 0);
            
                cSiric.prelievo(movimentoPrelievo);
                cSiric.stampaMovimenti();

                break;

            case 2:
                System.out.println("Inserisci l'importo da depositare: ");
                int importoDeposito = scanner.nextInt();
                Movimento movimentoDeposito = new Movimento(importoDeposito);
            
                movimentoDeposito.setDataRichiesta(0);
                movimentoDeposito.setValuta("Valuta: ", 0);
            
                cSiric.deposito(importoDeposito);
                cSiric.stampaMovimenti();
                break;

            default:
                System.out.println("Scelta non valida.");
        }
    }
}
	
