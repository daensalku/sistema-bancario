import java.time.LocalDate;

public class Movimento {
    private LocalDate[] dataRichiesta;
    private String[] valuta;
    private String[] descrizione;
    private String[] tipologia;
    private double[] importo;

    public Movimento(int size) {
        dataRichiesta = new LocalDate[size];
        valuta = new String[size];
        descrizione = new String[size];
        tipologia = new String[size];
        importo = new double[size];
    }

    public LocalDate getDataRichiesta(int index) {
        if (index >= 0 && index < dataRichiesta.length) {
            return dataRichiesta[index];
        } else {
            return null;
        }
    }

    public void setDataRichiesta(int index) {
        if (index >= 0 && index < this.dataRichiesta.length) {
            dataRichiesta[index] = LocalDate.now();
        } else {
            //X
        }
    }

    public String getValuta(int index){
        if (index >= 0 && index < valuta.length) {
            return valuta[index];
        } else {
            return null;
        }
    }

    public void setValuta(String valuta, int index) {
        if (index >= 0 && index < this.valuta.length) {
            this.valuta[index] = valuta;
        } else {
            //X
        }
    }

    public String getDescrizione(int index) {
        if (index >= 0 && index < descrizione.length) {
            return descrizione[index];
        } else {
            return null;
        }
    }

    public void setDescrizione(String descrizione, int index) {
        if (index >= 0 && index < this.descrizione.length) {
            this.descrizione[index] = descrizione;
        } else {
            //X
        }
    }

    public String getTipologia(int index) {
        if (index >= 0 && index < tipologia.length) {
            return tipologia[index];
        } else {
            return null;
        }
    }

    public void setTipologia(String tipologia, int index) {
        if (index >= 0 && index < this.tipologia.length) {
            this.tipologia[index] = tipologia;
        } else {
            //X
        }
    }

    public double getImporto(int index) {
        if (index >= 0 && index < importo.length) {
            return importo[index];
        } else {
            return 0.0;
        }
    }

    public void setImporto(double importo, int index) {
        if (index >= 0 && index < this.importo.length) {
            this.importo[index] = importo;
        } else {
            //X
        }
    }
}
